# DevOps Camp Webapp!

This is taken directly from [Django's Tutorial](https://docs.djangoproject.com/en/2.0/intro/tutorial01/).

It's a good example of a multi-tier webapp that can have complicated - or at least varying - configurations between development and production.  Plus, it's well documented over at djangoproject.com if you want more detail.

### Run this webapp

1. Install the dependencies included in the project
  - `pip3 install -r requirements.txt`
1. Change into the project directory
  - `cd mysite`
1. Add the models to the database
  - `python3 manage.py migrate`
1. Start the server
  - `python3 manage.py runserver`
1. [Check it out](http://localhost:8000/polls)


### Create an admin

1. Create an admin user
  - `python3 manage.py createsuperuser`
  - Enter a username (e.g. `admin` or `siteadmin`)
  - Enter an e-mail
  - Enter a password (twice)
1. [Do some adminning!](http://localhost:8000/admin)


### Use the admin site to create a poll!

### Run the automated test suite

1. `python3 manage.py test polls`
